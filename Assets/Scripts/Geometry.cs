﻿using UnityEngine;

public class Geometry
{
    static public Vector3 PointFromGrid(Vector2Int gridPoint)
    {
        var x = -3.5f + 1.0f * gridPoint.x;
        var z = -3.5f + 1.0f * gridPoint.y;
        return new Vector3(x, 0, z);
    }

    static public Vector2Int GridPoint(int col, int row)
    {
        return new Vector2Int(col, row);
    }

    static public Vector2Int GridFromPoint(Vector3 point)
    {
        var col = Mathf.FloorToInt(4.0f + point.x);
        var row = Mathf.FloorToInt(4.0f + point.z);
        return new Vector2Int(col, row);
    }
}
